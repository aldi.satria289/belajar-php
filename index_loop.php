<html>
    <head>
        <meta charset="utf-8">
        <title>Belajar PHP Dong</title>
    </head>
    <body>
    <h1>Loop PHP</h1>
    <?php 

        // for (variabelawal(mulai), batas(syarat, perubahan)
        for ($i = 0; $i < 5; $i++) 
        {
            echo "Belajar Loop <br> ------------ <br>";
        }
        echo "<br>";


        $nama = ['aldi', 'satria', 'budi', 'wow'];

        for ($i=0; $i < count($nama) - 2; $i++) // count($nama) -1 mengurangi 1
        {
            echo "for ".$nama[$i]. "<br>";
        }
        echo "<br>";

        //Foreach lebih mudah (keluar semua)
        foreach($nama as $h){
            echo "foreach ".$h. "<br>";
        }
        echo "<br>";

        //Foreach assosiative array
        $data = ['nama' => 'aldi', 'umur' => 24, 'sifat' => 'baik'];
        foreach ($data as $key => $value){
            echo "foreach ass ".$key. "<br>";
            echo "foreach ass ".$value. "<br>";
        }
        echo "<br>";

        //while , do while
        //while (syarat)
        $j = 0;
        $k = 0;
        while ($j < count($nama)){
            echo "while ".$nama[$j]. "<br>";
            $j++;
        }
        echo "<br>";

        do{
            echo "-------";
            echo "do while ".$nama[$k]. "<br>";
            $k++;
        }while ($k < count($nama));
        
        echo "<br>if else<br>";


        //Logika PHP 
        //Tipe data Boolean true false
        $hasil = true;
        $hasil2 = false;

        //if dan else if(kondisi/syarat){}
        $pass = '123';
        $pass2 = 123;

        if($pass == '1234'){ //$pass == $pass2
            echo 'Berhasil, password anda benar';
        }else{
            echo 'Gagal, password anda salah';
        }  
        echo "<br>";

        if($pass == $pass2){ 
            echo 'Berhasil, password anda benar';
        }else{
            echo 'Gagal, password anda salah';
        } 
        echo "<br>";

        //operator logika == === > >= < <= != (negasi/tidak sama dengan)
        if($pass === $pass2){ // === menguji tipe data string angka
            echo 'Berhasil, password anda benar';
        }else{
            echo 'Gagal, password anda salah';
        }  
        echo "<br><br>else if<br>";

        //else if lebih satu syarat
        $uangku = 1000;
        $mouse = 2000;
        $uangmu =5000;

        if ($uangku > $mouse){
            echo "Bisa beli";
        }else if($uangmu > $mouse){
            echo "1.Beli dari uangmu";
        }
        else{
            echo "Gak cukup";
        }
        echo "<br>";
        
        //if bercabang
        if ($uangku > $mouse){
            echo "Bisa beli";
        }else if($uangmu > $mouse){
            echo "2.Beli dari uangmu, ";

            if($uangmu >= $mouse *2){
                echo "Dibeliin 2 dari uangmu";
            }
        }
        else{
            echo "Gak cukup";
        }

        echo "<br><br>";
        
        //true atau false
        $hasil = true;
        $hasil2 = false;
        if (false){
            echo "True Bisa beli";
        }else if(false){
            echo "2.Beli dari uangmu, ";

            if($uangmu >= $mouse *2){
                echo "Dibeliin 2 dari uangmu";
            }
        }
        else{
            echo "False Gak cukup";
        }
        
        echo "<br><br>";

        // && atau || (&& 2 nya harus memenuhi syarat)
        if ($uangku > $mouse || $uangmu > $mouse){
            echo "|| boleh beli mouse";
        }else {
            echo "|| ga bisa beli";
        }
        echo "<br>";
        if ($uangku > $mouse && $uangmu > $mouse){
            echo "&& boleh beli mouse";
        }else {
            echo "&& ga bisa beli";
        }
        echo "<br><br>";

        //switch case
        $user = "Aldi";

        switch ($user) {
            case 'Satria':
                echo 'Namanya Satria';
                break;
            case 'Budi':
                echo 'Namanya Budi';
                break;
            case 'Aldis':
                echo 'Namanya Aldi';
                break;
            default:
                echo 'Switch case Salah semua';
        }
        
        echo "<br><br>";

        //Fungsi PHP function
        //Parameter
        function summon($coba, $nomor){
            $coba = "Nyoba Fungsi ". $coba. " ". $nomor;
            echo "------------";
            echo $coba;
            echo "------------";
        }
        
        function spasi(){
            echo "<br>";
        }

        summon("Test 1", 100);
        spasi();
        summon("Test 2", 200);
        spasi();
        summon("Test 3", 300);
        
        //Return
        function hitung($x, $y){
            $z = $x + $y;
            return $z;
        }
        $tambahan = hitung(2,5) * 10;

        spasi();
        spasi();
        echo "Return Jumlahnya ". hitung(2,4);
        spasi();
        echo "Return Jumlahnya ". $tambahan;

        spasi();
        spasi();
        //Scope
        $a = 2;
        $b = 3;

        function tambah(){
            global $a, $b; 
            $c = $a +$b; //$c = $GLOBALS['a'] + $GLOBALS['b'];
            return $c;
        }

        echo "Coba Scope ". tambah();
        spasi();
        spasi();

        //Anonymous Function
        $ngetik = function(){
            echo 'Anonymous Selamat datang';
        };
        $ngetik();
        
        spasi();

        $ngetik2 = function($kata){
            echo $kata;
        };
        $ngetik2('Anonymous Parameter Selamat datang');
        spasi();
        spasi();

        //Callback
        function teriak($callback){
            echo'Haloooo Callback <br>';
            $callback();
        }

        $panggil = function(){
            echo'Saya anonymous';
        };
        
        teriak($panggil);

        //echo print
        // print_r (untuk array) die var_dump 
        $car = 'mobil';
        echo $car ;
        spasi(); // die(); menghentikan script
        var_dump($car);
        spasi();


    ?>
    
    <br><br><br>Halo semua
    
    </body>
</html>