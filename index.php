<html>
    <head>
        <meta charset="utf-8">
        <title>Belajar PHP Dong</title>
    </head>
    <body>

    <?php 

    //Variabel
    $x = "Belajar PHP Tambah GIT";
    $n123 = "123";

    echo "<h1>$x</h1>"; 
    echo "Selamat";
    echo " Datang di ". $x . "<br>";
    echo "Nomor ". $n123. "<br><br><br>";


    //Integer dan Float
    $angka = 1000;
    $angka2 = 999.523;
    $angka3 = 5;
    $angka4 = 2;
    $angka3++; // +1  -- -1
    $tambah = $angka + $angka4;
    $angka = $angka + $angka3; // >> $angka += $angka3;

    //Matematik + - / * % ++ -- 
    echo $angka + $angka3 * $angka4 . "<br>";
    echo $angka3++ . "<br>";
    echo $tambah .  "<br>";
    echo $angka .  "<br><br><br>";

    //Metode Math round rand(min, max) max min
    echo round($angka2). "<br>";
    echo "Coba rand adalah nomor ". rand(1, 5). "<br>";
    echo "Coba max adalah nomor ". max(1, 10, 8, 7). " kalo min ". min(1, 10, 8, 7). "<br><br><br>"; // min

    //Metode String strlen && str_word_count
    //str_replace(find,replace,string)
    //str_repeat(text,times); str_shuffle(text)
    $text = "Hai semuanya";
    
    echo $text. "<br>";
    echo strlen($text). "<br>";
    echo str_replace("Hai ","Halo ",$text). "<br>";
    echo str_repeat("Hai ", 8). "<br>";
    echo str_repeat (str_replace("Hai ","Halo ",$text. "<br>"), 5). "<br><br>";

    //Array
    $kotak = array('kucing','kelinci','kelinci','kucing','macan');
    $nama = ['Aldi', 'Satria', 'Budi'];

    print_r ($kotak);
    echo "<br>";
    echo $kotak[0]. "<br>";
    echo $nama[2]. "<br>";
    echo $nama[0]." ".$nama[1]." ".$nama[2]. "<br>";

    //Metode Array array_unique _reverse shuffle count sort
    print_r( array_unique($kotak)); 
    echo "<br>";    
    print_r( array_reverse($kotak));
    echo "<br>";

    // shuffle($kotak); 
    // print_r($kotak);

    echo count($kotak). "<br><br><br>";  
    
    // sort($kotak);
    // print_r($kotak);

    //Associative array
    $data = array("nama" => "Aldi",
                   "umur" => "23",
                   "kerja" => "freelance"
                  );
    $data2 = array("nama2" => "Dwi",
                  "umur2" => "22",
                  "kerja2" => "istri"
                 );
    //Ubah variabel array
    $data ['nama'] = "Aldi Satria";
    //$data[1] = "21"; >> Kalau array biasa
    print_r ($data);
    echo "<br>";
    echo "Namanya adalah ". $data['nama']. " Umur ". $data['umur']. " Pekerjaan ".$data['kerja'];
    echo "<br><br><br>";
    
    //Metode Associative array_values array_keys array_merge
    print_r( array_values($data));
    echo "<br>";
    print_r( array_keys($data));    
    echo "<br>";
    print_r( array_merge($data, $data2));  
    echo "<br><br><br>";
    
    //Multidimensional array
    $coba = array(
                array("aldi","21","pintar"),
                array("budi","22","baik"),
                array("satria","23","ganteng")
            );
    // 00 01 02
    // 10 11 12
    // 20 21 22
    print_r($coba);
    echo "<br>";
    echo "Umur budi ". $coba[1][1]. "<br>";
    echo "Aldi itu  ". $coba[0][2]. "<br>";

    ?>
    
    <br><br><br>Halo semuanya
    
    </body>
</html>